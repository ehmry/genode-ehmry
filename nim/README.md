This directory hosts component submodules written in Nim. Integration is simple, 
commit a `Tupfile` containing the following to the upstream repository, assuming 
the repository contains an `*,nimble`, an `archives`, and a `runtime` file.

```tup
include_rules
include &(NIMBLE_BINARIES_INCLUDE)
include &(NIMBLE_PACKAGE_INCLUDE)
```
