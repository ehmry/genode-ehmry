{ ver=substr($7,0,6) }
{ tar="public/$(PUBLIC_USER)/pkg/"$6"/"ver".tar.xz" }
{ sig=tar".sig" }

{ print "all: " tar ".sig" }

{ print tar": depot/pkg/"$6"/current/.ARCHIVES" }
{ print "	@echo tar  $@" }
{ print "	@mkdir -p $(dir $@) "ver }
{ print "	@sed -f pkg.sed $< > "ver"/archives" }
{ print "	@cp $(dir $<)/runtime "ver }
{ print "	@tar cfJ $@ "ver }
{ print "	@rm -r "ver }
