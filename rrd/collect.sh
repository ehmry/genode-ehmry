#!/usr/bin/env bash

SQL_FILE=../.tup/db
RRD_FILE=.rrd

[ -e "$RRD_FILE" ] || rrdtool create "$RRD_FILE" \
	--step 1h \
	DS:node:GAUGE:2d:1000:1000000 \
	DS:normal_link:GAUGE:2d:1000:1000000 \
	DS:sticky_link:GAUGE:2d:1000:1000000 \
	DS:group_link:GAUGE:2d:1000:1000000 \
	DS:var:GAUGE:2d:1000:1000000 \
	RRA:AVERAGE:0.5:1h:72h \
	RRA:AVERAGE:0.5:1d:4y \

QUERY="select COUNT(*) from node; select COUNT(*) from normal_link; select COUNT(*) from sticky_link;  select COUNT(*) from group_link;  select COUNT(*) from var;"

LINES=`sqlite3 $SQL_FILE -readonly "$QUERY" `
RECORDS=`echo $LINES | tr ' ' ':' `

rrdtool update ${RRD_FILE} N:$RECORDS


rrdtool graph chart.png \
	--start 1561669840 \
	--logarithmic \
	DEF:nodes="$RRD_FILE":node:AVERAGE LINE2:nodes#000000:nodes \
	DEF:normal_link="$RRD_FILE":normal_link:AVERAGE LINE2:normal_link#0000FF:"normal links" \
	DEF:sticky_link="$RRD_FILE":sticky_link:AVERAGE LINE2:sticky_link#0080FF:"sticky links" \
	DEF:group_link="$RRD_FILE":group_link:AVERAGE LINE2:group_link#8000FF:"group links" \
	DEF:var="$RRD_FILE":var:AVERAGE LINE2:var#FF0000:variables \
