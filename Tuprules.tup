.gitignore

ifndef TOOL_CHAIN_PREFIX
	error TOOL_CHAIN_PREFIX not configured, missing variant?
endif

ifeq (@(TUP_ARCH),i386)
CC_MARCH = -march=i686 -m32
LD_MARCH = -melf_i386
AS_MARCH = -march=i686 --32
endif

ifeq (@(TUP_ARCH),x86_64)
CC_MARCH = -m64 -mcmodel=large
LD_MARCH = -melf_x86_64
endif

ifeq (@(TUP_ARCH),arm_v8)
CC_MARCH = -march=armv8-a
endif

GENODE_DIR = $(TUP_CWD)/upstream
&STUB_DIR = stub
STUB_DIR = $(TUP_CWD)/stub

export NIX_PATH
NIX_BUILD = nix-build --option substitute false

LIBGCC = `$(CC) $(CC_MARCH) -print-libgcc-file-name`

!prg = | $(GENODE_DIR)/<stub> $(GENODE_DIR)/<pkg-config> \
|> ^o LD %o^ \
	$(LD) $(LD_MARCH) $(LDFLAGS) \
	-L&(STUB_DIR) \
	`$(PKG_CONFIG) --libs $(LIBS) genode-prg` \
	%f \
	$(LIBGCC) \
	-o %o \
|> %d

!lib = | $(GENODE_DIR)/<stub> $(GENODE_DIR)/<pkg-config> |> ^o LD %o^ $(LD) $(LD_MARCH) %f $(LDFLAGS) `$(PKG_CONFIG) --libs genode-lib $(LIBS)` -L&(STUB_DIR) -o %o |>

ifeq ($(RAW_NAME),)
RAW_NAME = $(TARGET_NAME)
endif

ifeq ($(BIN_NAME),)
BIN_NAME = $(TARGET_NAME)
endif

ifeq ($(PKG_NAME),)
PKG_NAME = $(TARGET_NAME)
endif

GIT_VERSION=`git describe --always`
BIN_VERSION=$(GIT_VERSION)
PKG_VERSION=$(GIT_VERSION)

ifeq ($(RAW_VERSION),)
RAW_VERSION=$(GIT_VERSION)
endif

DEPOT_DIR  = $(TUP_CWD)/depot
DEPOT_RAW_DIR = $(DEPOT_DIR)/raw
DEPOT_BIN_DIR = $(DEPOT_DIR)/bin
DEPOT_PKG_DIR = $(DEPOT_DIR)/pkg

PUBLIC_DIR = $(TUP_CWD)/public
PUBLIC_RAW_DIR = $(PUBLIC_DIR)/raw
PUBLIC_BIN_DIR = $(PUBLIC_DIR)/bin
PUBLIC_PKG_DIR = $(PUBLIC_DIR)/pkg

RAW_DIR = $(DEPOT_RAW_DIR)/$(RAW_NAME)/current
	# Destination for locally defined raw package

BIN_DIR = $(DEPOT_BIN_DIR)/$(BIN_NAME)/current
	# Destination for locally defined binary package

PKG_DIR = $(DEPOT_BIN_DIR)/$(BIN_NAME)/current
	# Destination for locally defined runtime package

!raw = |> ^ generate raw metadata^ \
	echo $(RAW_DEPENDS) local/raw/$(RAW_NAME)/$(RAW_VERSION) | tr ' ' '\n' > %o; \
	echo local/raw/$(RAW_NAME)/$(RAW_VERSION) > %o \
|> $(RAW_DIR)/.ARCHIVES $(DEPOT_DIR)/<raw-archives>

!bin = |> ^ generate bin metadata^ \
	echo $(BIN_DEPENDS) local/src/$(BIN_NAME)/$(BIN_VERSION) | tr ' ' '\n' > %o; \
|> $(BIN_DIR)/.ARCHIVES $(DEPOT_DIR)/<bin-archives>
	# Macro invoked in BIN_RULES

&RAW_RULES = errata/raw.tup
	# Rules for finalizing a locally defined raw package
&BIN_RULES = errata/bin.tup
	# Rules for finalizing a locally defined binary package
&PKG_RULES = errata/pkg.tup
	# Rules for finalizing a locally defined runtime package

AWK_LOCAL_ARCHIVES = awk -F '/' \
	'{if ($1 != "local") {print $0} \
		else {print $1"/"$2"/"$3"/current"}}'

!pkg = |> |>

!collect_raw = |> ^ COLLECT %b^ \
	cp %f %o \
|> $(RAW_DIR)/%b

!collect_bin = |> ^ COLLECT %b^ \
	@(TOOL_CHAIN_PREFIX)strip -o %o %f \
|> $(DEPOT_BIN_DIR)/$(BIN_NAME)/current/%b

!collect_pkg_runtime = | $(VERSIONS_SED_FILE) |> ^ package %f^ \
	xmllint --noout -schema $(ERRATA_DIR)/runtime.xsd %f; \
	cp %f $(DEPOT_PKG_DIR)/$(PKG_NAME)/current/runtime; \
	echo $(PKG_DEPENDS) | tr ' ' '\n' | sed \
			-e 's|_/\(.*\)/\(.*\)$|local/\1/\2/current|' \
		> $(DEPOT_PKG_DIR)/$(PKG_NAME)/current/archives; \
	sed -f $(VERSIONS_SED_FILE) $(DEPOT_PKG_DIR)/$(PKG_NAME)/current/archives \
		> $(DEPOT_PKG_DIR)/$(PKG_NAME)/current/.ARCHIVES; \
|> \
	$(DEPOT_PKG_DIR)/$(PKG_NAME)/current/runtime \
	$(DEPOT_PKG_DIR)/$(PKG_NAME)/current/archives \
	$(DEPOT_PKG_DIR)/$(PKG_NAME)/current/.ARCHIVES \
	$(DEPOT_DIR)/<packages>

!collect_pkg = |> ^ COLLECT %b^ cp %f %o \
	|> $(DEPOT_PKG_DIR)/$(PKG_NAME)/current/%b \
		$(DEPOT_DIR)/<packages>

VERSIONS_SED_FILE = $(TUP_CWD)/versions.sed
PKG_SED_FILE = $(TUP_CWD)/pkg.sed

RAW_LISTING = $(TUP_CWD)/raw-list
BIN_LISTING = $(TUP_CWD)/bin-list
PKG_LISTING = $(TUP_CWD)/pkg-list

SDK_INCLUDE = $(TUP_CWD)/sdk.tup

ERRATA_DIR = $(TUP_CWD)/errata

export PKG_CONFIG_PATH
PKG_CONFIG = PKG_CONFIG_PATH=$(GENODE_DIR)/upstream:pkg-config:$PKG_CONFIG_PATH pkg-config

CC = @(TOOL_CHAIN_PREFIX)gcc
CXX = @(TOOL_CHAIN_PREFIX)g++
LD= @(TOOL_CHAIN_PREFIX)ld
AR = @(TOOL_CHAIN_PREFIX)ar
AS = @(TOOL_CHAIN_PREFIX)as
OBJCOPY = @(TOOL_CHAIN_PREFIX)objcopy

AS_OPT += $(AS_MARCH)

LIBGCC = `$(CC) $(CC_MARCH) -print-libgcc-file-name`

PKG_CONFIG_DIR = $(TUP_CWD)/pkg-config
&PKG_CONFIG_DIR = pkg-config

PKG_CONFIG_PATH = $(PKG_CONFIG_DIR)
PKG_CONFIG = PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config

!asm = |> ^ CC %b^ @(CC_WRAPPER) $(CC) $(DEFINES) $(CFLAGS) $(CFLAGS_%e) $(CFLAGS_%f) -D__ASSEMBLY__ $(INCLUDES) -c %f -o %o |> %B.o

!cc = | $(GENODE_DIR)/<pkg-config> |> ^o CC %f^ @(CC_WRAPPER) $(CC) $(DEFINES) $(CFLAGS) $(CFLAGS_%e) $(CFLAGS_%f) `$(PKG_CONFIG) --cflags $(LIBS) $(LIBS_CFLAGS)` $(INCLUDES) -c %f -o %o |> %B.o

!cc_port = | $(GENODE_DIR)/<pkg-config> |> ^o CC %o^ @(CC_WRAPPER) $(CC) -c -fPIC $(DEFINES) $(CFLAGS) $(CFLAGS_%o) $(INCLUDES) `$(PKG_CONFIG) --cflags $(LIBS) $(LIBS_CFLAGS)` -o %o |> %B.o

!cxx = | $(GENODE_DIR)/<pkg-config> |> ^o CXX %b^ @(CC_WRAPPER) $(CXX) $(DEFINES) $(CXXFLAGS) $(CXXFLAGS_%e) $(CXXFLAGS_%f) `$(PKG_CONFIG) --cflags $(LIBS)` $(INCLUDES) -c %f -o %o |> %B.o

!cxx_port = | $(GENODE_DIR)/<pkg-config> |> ^o CXX %o^ @(CC_WRAPPER) $(CXX) -c $(DEFINES) $(CXXFLAGS) $(CXXFLAGS_%f) `$(PKG_CONFIG) --cflags $(LIBS)` $(INCLUDES) -o %o |> %B.o

!ld = | $(GENODE_DIR)/<lib> $(GENODE_DIR)/<pkg-config> |> ^o LD %o^ $(LD) $(LDFLAGS) `$(PKG_CONFIG) --libs $(LIBS)` %f -o %o |>

!strip = |> strip -o %o %f |>

&LD_SCRIPT_SO = upstream/repos/base/src/ld/genode_rel.ld

ifeq (@(TUP_ARCH),x86_64)
	ASM_SYM_DEPENDENCY = movq \1@GOTPCREL(%rip), %rax
else
	ASM_SYM_DEPENDENCY = .long \1
endif

!abi_stub = |> ^ STUB %o^\
	sed \
		-e "s/^\(\w\+\) D \(\w\+\)\$/.data; .global \1; .type \1,%%object; .size \1,\2; \1:/p" \
		-e "s/^\(\w\+\) V/.data; .weak \1; .type \1,%%object; \1:/p" \
		-e "s/^\(\w\+\) T/.text; .global \1; .type \1,%%function; \1:/p" \
		-e "s/^\(\w\+\) R \(\w\+\)\$/.section .rodata; .global \1; .type \1,%%object; .size \1,\2; \1:/p" \
		-e "s/^\(\w\+\) W/.text; .weak \1; .type \1,%%function; \1:/p" \
		-e "s/^\(\w\+\) B \(\w\+\)\$/.bss; .global \1; .type \1,%%object; .size \1,\2; \1:/p" \
		-e "s/^\(\w\+\) U/.text; .global \1; $(ASM_SYM_DEPENDENCY)/p" \
		%f \
	| $(CC) -x assembler -c - -o tmp.o; \
	$(LD) -o %o \
		-shared \
		-T &(LD_SCRIPT_SO) \
		tmp.o; \
	rm tmp.o; \
|> $(STUB_DIR)/%B.lib.so $(GENODE_DIR)/<stub>
