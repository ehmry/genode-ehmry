#!/usr/bin/env -S awk -f
BEGIN {
	print ".section .data.boot_modules_binaries";
	print "";
	print ".global _boot_modules_binaries_begin";
	print "_boot_modules_binaries_begin:";
	print "";
}

{
	print ".p2align MIN_PAGE_SIZE_LOG2";
	print "_boot_module_"NR"_begin:";
	print ".incbin \""$0"\"";
	print "_boot_module_"NR"_end:";
	print "";
}

END {
	print ".p2align MIN_PAGE_SIZE_LOG2";
	print ".global _boot_modules_binaries_end";
	print "_boot_modules_binaries_end:";
	print "";
}
