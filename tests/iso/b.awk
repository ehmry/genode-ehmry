#!/usr/bin/env -S awk -f

BEGIN {
	FS = "/"

}

{
	print ".p2align DATA_ACCESS_ALIGNM_LOG2";
	print "_boot_module_"NR"_name:";
	print ".string \""$(NF)"\"";
	print ".byte 0";
	print "";
}
