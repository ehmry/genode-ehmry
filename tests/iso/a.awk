#!/usr/bin/env -S awk -f
BEGIN {
	print ".set MIN_PAGE_SIZE_LOG2, 12";
	print ".set DATA_ACCESS_ALIGNM_LOG2, 3";
	print "";
	print ".section .data";
	print "";
	print ".p2align DATA_ACCESS_ALIGNM_LOG2";
	print ".global _boot_modules_headers_begin";
	print "_boot_modules_headers_begin:";
	print "";
}

{
	print ADDR_TYPE" _boot_module_"NR"_name";
	print ADDR_TYPE" _boot_module_"NR"_begin";
	print ADDR_TYPE" _boot_module_"NR"_end - _boot_module_"NR"_begin";
	print "";
}

END {
	print ".global _boot_modules_headers_end";
	print "_boot_modules_headers_end:";
	print "";
}
