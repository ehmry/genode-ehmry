![UNDER CONSTRUCTION](.underconstruction.gif)

This is a super-repository containing an experimental [Tup](http://gittup.org/tup) build 
system and submodules containing Genode components and runtime metadata.

See http://github.com/ehmry/genode-tup-super for a super-repo skeleton, please 
forward any pull requests for the build-system there.
