#!/bin/sh

LINK=$1
shift

for LIST in $@
do
	grep '\.c$' ${LIST} | while read f
	do
		echo ": {port} |> !cc_port ${LINK}/${f} |> ${f}.o {obj}"
	done
	grep '\.S$' ${LIST} | while read f
	do
		echo ": {port} |> !cc_port ${LINK}/${f} |> ${f}.o {obj}"
	done
done
