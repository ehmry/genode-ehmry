{ preparePort, mpg123 }:

let
  apiVersion = 44; # found in configure.ac
in
preparePort rec {
  inherit (mpg123) name src;
  outputs = [ "source" "api" ];
  buildPhase =
    ''
      mkdir -p $api/include $api/pkg-config

      sed \
        -e 's/@API_VERSION@/$(API_VERSION)/' \
        -e '/@.*@/d' \
        src/libmpg123/mpg123.h.in > $api/include/mpg123.h

      cp src/libmpg123/fmt123.h $api/include

      sed \
        -e "s|@includedir@|$api/include|" \
        -e 's/@PACKAGE_VERSION@/${(builtins.parseDrvName name).version}/' \
        -e 's/^Libs:.*/Libs: -l:mpg123.lib.so/' \
        -e '/@/d' \
        < libmpg123.pc.in > $api/pkg-config/mpg123.pc
    '';
}
