{ tool ? import ../tool { } }:

let
  importPort = path:
    let f = (import path);
    in f (builtins.intersectAttrs (builtins.functionArgs f) (tool.nixpkgs // tool) );

  dir = builtins.readDir ../ports;
in
builtins.listToAttrs (
  builtins.filter
    (x: x != null)
    (map
      (name:
         if (builtins.getAttr name dir) != "directory" then null else
           { inherit name;
             value = importPort (../ports + "/${name}");
           }
      )
      (builtins.attrNames dir)
    )
)
