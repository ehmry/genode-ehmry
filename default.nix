{ pkgs ? import <nixpkgs> {} }: with pkgs;

let
  lddir = ./upstream/repos/base/src/ld;
  toolchain = import ./errata/toolchain.nix { inherit pkgs; };
in
pkgs.mkShell {
  name = "genode-dev-env";
  nativeBuildInputs =
    [ toolchain
      stdenv.glibc.dev
      git tup
      ccache
      tcl
      gnumake
      which
      findutils
      autoconf264
      autogen
      cdrtools

      expect libxml2 syslinux qemu xorriso

      # libc
      flex bison

      # virtualbox
      yasm libxslt iasl

      # qt5
      gperf
      qt5.full

      # ncurses
      mawk

      # fb_sdl
      pkgconfig
      SDL.dev
      alsaLib.dev

      # ncurses port needs infocmp
      ncurses
    ];

  passthru = { inherit lddir toolchain; };

  shellHook =
    ''
      export LD_DIR=${lddir}
      export TOOLCHAIN_DIR=${toolchain}
      export SYSLINUX=${syslinux}
      export PROMPT_DIRTRIM=2
      export PS1="\[\033[1;30m\]Genode-dev [\[\033[1;37m\]\w\[\033[1;30m\]] $\[\033[0m\] "
      export PS2="\[\033[1;30m\]>\[\033[0m\] "
    '';
      # Setup a prompt with a distinct appearance
}
